from flask import Flask, request
import json


app = Flask(__name__)

######  loadming data   #######
data = json.load(open("data.json"))
CUSTOMERDATA = {}
for i in data:
    CUSTOMERDATA[i["customer_id"]] = i
################################


@app.route("/")
def home():
    return "Hello, World!"

@app.route("/iseligible",methods = ['POST',])
def isEligible():
    body = request.get_json()
    if("customer_id" not in body):
        return {
            "ErrorCode"  : "001",
            "msg" : " 'customer_id' need to be passsed"
        }
    cid = body["customer_id"]
    if(cid in CUSTOMERDATA):
        return CUSTOMERDATA[cid]
    else:
        return {
            "ErrorCode"  : "002",
            "msg" : "Customer Not Found"
        }

    
    
if __name__ == "__main__":
    app.run(debug=True)
